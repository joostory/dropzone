(function($) {
    var $document = $(document),
        $window = $(window);

    $.Dropzone = function(zone, input, handlers) {
	var $zone = $(zone),
	    $input = $(input);

        handlers = handlers || {};

        var eventHandler = {
            dragEnter: handlers.dragEnter || function(e) {
                console.log("default dragEnter handler")
            },
            dragLeave: handlers.dragLeave || function(e) {
                console.log("default dragLeave handler")
            },
            drop: handlers.drop || function(files) {
                console.log("default drop handler", files.length);
                $.each(files, function(i, file) {
                    console.log(file.name, file.type, file.size)
                })
            }
        }
	var dragCounter = 0;

	if (!(window.File && window.FileList && window.Blob && window.FormData)) {
	    throw new Error("not supported");
	}

        if ($input.length > 0) {
            $input.on("change", function(e) {
                eventHandler.drop($input[0].files);
                $input.val("");
            });
        }

	$zone.on("drop", function(e) {
            if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length > 0) {
                eventHandler.drop(e.originalEvent.dataTransfer.files);
            }
            e.stopPropagation();
	    e.preventDefault();
	}).on("dragover", function(e) {
	    e.stopPropagation();
	    e.preventDefault();
	});

	$window.on("dragenter", function(e) {
	    dragCounter++;
            if (dragCounter == 1) {
                eventHandler.dragEnter(e);
            }
	}).on("dragleave", function(e) {
	    dragCounter--;
	    if (dragCounter == 0) {
                eventHandler.dragLeave(e);
	    }
	});

	return {}
    }
})(jQuery);
